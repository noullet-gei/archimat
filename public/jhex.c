/*
   gcc -Wall -o jhex.exe jhex.c
   programme de conversion de fichiers "binaire <--> texte ascii hexa"
   tout en ligne de commande
 */

#include <stdio.h>
#include <stdlib.h>
#include <fcntl.h>
#include <io.h>
#include <sys\stat.h>                         /*DOS*/

#define BUFLEN 16384

int srchand, desthand;
char sbuf[BUFLEN];
char dbuf[BUFLEN];
int  sind;                       /* index courant dans sbuf[]  */
int  dind;                       /* index courant dans dbuf[]  */
int sbufcont;                    /* contenu de sbuf[] */
int fin = 0;
int lncnt=0, lnmax;              /* gestion longueur lignes texte */

/* -------------------------- les acces aux fichiers -------------- */
int getbuf()
{ return( read( srchand, sbuf, BUFLEN ) ); }

void putbuf()
{
int wrcont;
wrcont = write( desthand, dbuf, dind );
if ( wrcont < dind )
   { printf("erreur ecriture disque\n"); close(desthand); exit(1); };
dind = 0;
}

void ouvre( char *srcpath, char * destpath )
{
srchand = open( srcpath, O_RDONLY | O_BINARY );       /*DOS*/
if ( srchand <= 0 )  { printf("%s non ouvert \n", srcpath ); exit(1); }
else  {
      printf("%s ouvert en lecture\n", srcpath );
      desthand = open( destpath, O_RDWR | O_BINARY | O_CREAT | O_TRUNC, /*DOS*/
                       S_IREAD | S_IWRITE );
      if ( desthand <= 0 ) { printf("%s non ouvert \n", destpath ); exit(1); }
      else  { printf("%s ouvert en ecriture\n", destpath ); };
      sbufcont = getbuf(); sind = 0;
      };
dind = 0;
}

void ferme()
{
close(srchand);
if (dind) putbuf();
close(desthand);
}

void putbyte( thebyte )
char thebyte;
{  dbuf[dind++] = thebyte; if ( dind >= BUFLEN ) putbuf();  }

char getbyte()  /* fin mis a 1 <==> ce byte est le dernier du fichier */
{
char thebyte;
thebyte = sbuf[sind++];
if ( sind >= sbufcont )
   {
   if   ( sbufcont < BUFLEN ) { fin++;  }
   else {
        sbufcont = getbuf();
        if ( sbufcont == 0 ) fin++;
        sind = 0;
        };
   };
return(thebyte);
}

/* ----------------------------- les conversions ------------------ */

/* conversion hexa -> binaire
   hdigit rend -1 si digit ok, 0 sinon, et rend 4 bits dans *nibble  */

int hdigit( nibble, locar )
int *nibble; char locar;
{
int hexflag;
if ( locar >= 'a' ) locar -= 32;
*nibble = locar - '0';
hexflag = ( ( locar >= 'A' ) && ( locar <= 'F' ) );
if ( hexflag ) *nibble -= 7;
hexflag = hexflag || ( ( locar >= '0' ) && ( locar <= '9' ) );
return(hexflag);
}

int gethexbyte()     /* lit un byte hexa dans le fichier d'entree */
{                    /* saute les blancs entre 2 bytes */
int hi, lo, good;
do {
   good = hdigit( &hi, getbyte() );
   } while (  ( good == 0 ) && (!fin) );
if (fin) { ferme(); exit(0); };
if ( hdigit( &lo, getbyte() ) == 0 )
   { printf("erreur hexa\n"); ferme(); exit(1);  };
return( ( hi << 4 ) + lo );
}

void fromhex()
{
do {
   putbyte( gethexbyte() );
   } while (!fin);
}


/* conversion binaire -> hexa */

void bytetohex( thebyte )
char thebyte;
{
char locar;
locar = ( thebyte >> 4 ) & 15;
putbyte(  ( (locar>9)?(locar+7):(locar) ) + '0' );
locar = thebyte & 15;
putbyte(  ( (locar>9)?(locar+7):(locar) ) + '0' );
}

void tohex()
{
lnmax = 64;   /* 32 bytes par ligne */
lncnt = 0;
do {
   bytetohex( getbyte() );
   lncnt += 2;
   if ( lncnt >= lnmax )
      {
      putbyte(13); putbyte(10);     /*DOS*/
      lncnt = 0;
      };
   } while (!fin);
}

void tohexsp()
{
lnmax = 72;   /* 24 bytes par ligne */
lncnt = 0;
do {
   bytetohex( getbyte() );
   lncnt += 3;
   if   ( lncnt >= lnmax )
        {
        putbyte(13); putbyte(10);     /*DOS*/
        lncnt = 0;
        }
   else {
        putbyte(' ');
        };
   } while (!fin);
}

void tohexspa() /* hexa avec ascii correspondant */
{
unsigned char lbuf [16]; int i, im;
lnmax = 48;   /* 16 bytes par ligne */
lncnt = 0; i = 0;
do {
   bytetohex( lbuf[i++] = getbyte() );
   lncnt += 3;
   putbyte(' ');
   if   ( lncnt >= lnmax )
        {
        putbyte(' ');
        for ( i = 0; i < 16; i++ )
            putbyte( ( (lbuf[i]>=' ') && (lbuf[i]<='~') )?(lbuf[i]):(' ') );
        putbyte(13); putbyte(10);     /*DOS*/
        lncnt = 0; i = 0;
        }
   } while (!fin);
if ( i > 0 )
   {
   while ( lncnt < lnmax ) { putbyte(' '); lncnt++;  }
   im = i;
   putbyte(' ');
   for ( i = 0; i < im; i++ )
       putbyte( ( (lbuf[i]>=' ') && (lbuf[i]<='~') )?(lbuf[i]):(' ') );
   putbyte(13); putbyte(10);     /*DOS*/
   }
}

void usage()
{
printf("usage : jhex option input_file output_file\n"); 
printf("-t --> to hexa             ( 32 bytes / line )\n");
printf("-T --> to hexa with spaces ( 24 bytes / line )\n");
printf("-a --> to hexa with ascii  ( 16 bytes / line )\n");
printf("-f --> from hexa\n");
exit(1);
}

int main( int argc, char ** argv )
{
if ( argc != 4 )
   usage();
if ( argv[1][0] != '-' )
   usage();
ouvre( argv[2], argv[3] );
switch( argv[1][1] )
   {
   case 't' : tohex(); break;
   case 'T' : tohexsp(); break;
   case 'a' : tohexspa(); break;
   case 'f' : fromhex(); break;
   default : usage();
   }
ferme();
return 0;
}
